<?php

namespace Pon\LaravelSms\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static bool send(string $mobile, string $sceneId = '', int $length = 6) 发送短信验证码到指定手机
 * @method static bool check(int $captcha, string $mobile, string $sceneId = '') 验证提供的验证码是否正确
 * @method static array getConfig() 获取当前配置
 * @method static \Pon\LaravelSms\Handler\Sms setExpires(int $minutes) 设置有效时长
 * @method static \Pon\LaravelSms\Handler\Sms setEasySmsConfig(array $easySmsConfig) 设置EasySms扩展配置并返回当前实例
 * @method static array getEasySmsConfig() 获取EasySms扩展配置
 * @method static \Pon\LaravelSms\Handler\Sms setTemplateId(int|string $templateId) 设置模板ID并返回当前实例
 * @method static string getTemplateId() 获取当前设置的模板ID，若未设置则抛出异常
 * @method static \Pon\LaravelSms\Handler\Sms setConfig(array $config) 设置全量配置并返回服务实例以支持链式调用
 */
class Sms extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Pon\LaravelSms\Handler\Sms::class;
    }
}
