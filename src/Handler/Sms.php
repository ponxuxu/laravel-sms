<?php

namespace Pon\LaravelSms\Handler;

use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\InvalidArgumentException;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;
use Overtrue\EasySms\Strategies\OrderStrategy;
use Pon\LaravelSms\Exceptions\CacheException;
use Pon\LaravelSms\Exceptions\CaptchaException;
use Pon\LaravelSms\Exceptions\ConfigException;

class Sms
{
    /**
     * @var array 配置
     */
    private array $config = [];

    private array $easySmsConfig = [];

    /**
     * @var int|string 模板ID
     */
    private int|string $templateId;

    /**
     * @throws ConfigException
     */
    public function __construct(array $options = [])
    {
        $options = array_merge(config('sms'), $options);

        if (empty($options['app_id'])) {
            throw new ConfigException('未配置 app_id');
        }

        if (empty($options['account_sid'])) {
            throw new ConfigException('未配置 account_sid');
        }

        if (empty($options['account_token'])) {
            throw new ConfigException('未配置 account_token');
        }

        $easySmsConfig = [
            'timeout' => 5.0,
            'default' => [
                'strategy' => OrderStrategy::class,
                'gateways' => ['yuntongxun']
            ],
            'gateways' => [
                'errorlog' => ['file' => '/tmp/easy-sms.log'],
                'yuntongxun' => [
                    'app_id' => $options['app_id'],
                    'account_sid' => $options['account_sid'],
                    'account_token' => $options['account_token'],
                    'is_sub_account' => false,
                ],
            ],
        ];

        $this->setConfig($options);
        $this->setEasySmsConfig($easySmsConfig);
        $this->setTemplateId($options['template_id'] ?? '');
    }

    /**
     * @desc 设置全量配置
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config): static
    {
        $this->config = array_merge($this->config, $config);

        return $this;
    }

    /**
     * @desc 获取全量配置
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @desc 设置有效时长
     * @param int $minutes
     * @return $this
     */
    public function setExpires(int $minutes): static
    {
        $this->setConfig(['expires' => $minutes]);

        return $this;
    }

    /**
     * @desc 设置esaysms扩展配置
     * @param array $easySmsConfig
     * @return $this
     */
    public function setEasySmsConfig(array $easySmsConfig): static
    {
        $this->easySmsConfig = $easySmsConfig;

        return $this;
    }

    /**
     * @desc 获取easysms扩展配置
     * @return array
     */
    public function getEasySmsConfig(): array
    {
        return $this->easySmsConfig;
    }

    /**
     * 设置模板ID
     * @param $templateId
     * @return $this
     */
    public function setTemplateId($templateId): static
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * 获取模板ID
     * @throws ConfigException
     */
    public function getTemplateId(): string
    {
        if (empty($this->templateId)) {
            throw new ConfigException('sms template_id 未设置');
        }

        return $this->templateId;
    }

    /**
     * 发送验证码
     * @throws NoGatewayAvailableException
     * @throws ConfigException
     * @throws InvalidArgumentException|CacheException|CaptchaException
     */
    public function send(string $mobile, string $sceneId = '', int $length = 6): bool
    {
        $captcha = $this->createCaptcha($length);
        $cache = new SmsCache($this->getConfig());
        $cache = $cache->setMobile($mobile)->setSceneId($sceneId);

        if ($ttl = $cache->validateInterval()) {
            throw new CacheException('短信验证码获取过于频繁，请' . $ttl . '秒后再试', 410016);
        }

        if (!$cache->validateMobileSmsDailyMax()) {
            throw new CacheException('短信验证码获取次数超过当日限制', 410016);
        }

        $easySms = new EasySms($this->getEasySmsConfig());
        $result = $easySms->send($mobile, [
            'template' => $this->getTemplateId(),
            'data' => [$captcha, $cache->getExpires() . '分钟'],
        ]);

        if (empty($result['yuntongxun']['result']['statusCode']) || $result['yuntongxun']['result']['statusCode'] != '000000') {
            $errMsg = $result['yuntongxun']['result']['statusMsg'] ?? '短信发送失败';
            throw new CaptchaException($errMsg);
        }

        return $cache->cacheCaptcha($captcha);
    }

    /**
     * 验证验证码是否正确
     * @throws CacheException
     * @throws CaptchaException
     */
    public function check(int $captcha, string $mobile, string $sceneId = ''): bool
    {
        $cache = new SmsCache($this->getConfig());
        $sceneId = $sceneId ?: $cache->getSceneId();
        $cache = $cache->setMobile($mobile)->setSceneId($sceneId);

        if (!$cache->validateCaptcha($captcha)) {
            throw new CaptchaException('短信验证码错误');
        }

        return true;
    }

    /**
     * 生成随机验证码
     * @param int $length 验证码长度
     * @return string
     */
    public function createCaptcha(int $length = 6): string
    {
        for ($captcha = '', $i = 0; $i < $length; $i++) {
            $min = empty($captcha) ? 1 : 0;
            $captcha .= mt_rand($min, 9);
        }

        return $captcha;
    }

}
